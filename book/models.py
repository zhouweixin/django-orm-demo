from django.db import models


# 定义图书模型, 必须继承models.Model
class Book(models.Model):
    # id: 主键, 自增长
    id = models.AutoField(primary_key=True)
    # name: varchar(100), 非空
    name = models.CharField(max_length=100, null=False)
    # author: varchar(100)
    author = models.CharField(max_length=100, null=False)
    # price: float
    price = models.FloatField(null=False, default=0)


class Publisher(models.Model):
    # 未设置主键时, orm会自动添加名为id的自增长主键
    name = models.CharField(max_length=100, null=False)
    address = models.CharField(max_length=100, null=False)


# 自动把模型生成表的流程:

# 1.使用makemigrations生成迁移脚本
# python manage.py makemigrations

# 2.使用migrate将新生成的迁移脚本文件映射到数据库中
# python manage.py migrate

